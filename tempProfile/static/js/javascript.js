var acc = document.getElementsByClassName("accordion");
var i;
var clicked = true;
for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}
function myFunction() {
    if(clicked){
        document.body.style.backgroundColor = "rgba(19, 31, 206, 0.44)";
        clicked = false;
    }
    else{
        document.body.style.backgroundColor = "rgba(206,19,48,0.12)";
        clicked = true;
    }
}

$.ajax('/request/')
    .done(function (data) {
        data = data['books'];
        var tbody = '';
        for (var i = 0; i < data.length; i++) {
            tbody = tbody
                + '<tr id=\"'+ data[i].id +'\" style="text-align: center">'
                    + '<td><img src=\"'+ data[i].image +'\"</td>'
                    + '<td>' + data[i].author + '</td>'
                    + '<td>' + data[i].title + '</td>'
                    + '<td>' + data[i].publisher + '</td>'
                    + '<td><img onclick=\"on(\''+ data[i].id +'\')\" src=\"../static/images/unfav.png\" style="width: 30px;"></td>';
                }
        $(tbody).appendTo('#table');
    });

    function on(id) {
        var img = $('#' + id + ' td img');
        var imgSource = img[1].src;
        if (imgSource.includes('unfav')) {
            img[1].src = '../static/images/fav.png';
        } else {
            img[1].src = '../static/images/unfav.png';
        }
    }
