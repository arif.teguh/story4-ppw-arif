from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from django.http import HttpRequest
from datetime import date
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

class Test(TestCase):
	def test_url_exist(self):
		response = self.client.get('')
		self.assertEqual(response.status_code,200)
		
	def test_main_template(self):
		response = self.client.get('')
		self.assertTemplateUsed(response,'story3.html')

	def test_using_index_func(self):
		found = resolve('/')
		self.assertEqual(found.func, index)

	def test_book_template(self):
		response = self.client.get('/book/')
		self.assertTemplateUsed(response,'Book.html')

	

	
  
		
		
	
		