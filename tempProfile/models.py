from django.db import models

# Create your models here.
 
 
class Jadwal(models.Model):
	Monday = 'Mon'
	Tuesday = "Tue"
	Wednesday = "Wed"
	Thursday = "Thu"
	Friday = "Fri"
	Saturday = "Sat"
	Sunday = "Sun"
	namaHari = (
		(Monday, "Monday"),
		(Tuesday, "Tuesday"),
		(Wednesday,"Wednesday"),
		(Thursday,"Thursday"),
		(Friday,"Friday"),
		(Saturday,"Saturday"),
		(Sunday,"Sunday")
		)
	tanggal = models.DateField()
	hari = models.CharField(max_length=3, choices = namaHari)
	jam = models.TimeField()
	kegiatan =models.CharField(max_length=100)
	tempat = models.CharField(max_length=20)
	kategori = models.CharField(max_length=20)
	
