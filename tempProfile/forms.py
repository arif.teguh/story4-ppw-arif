from django import forms
from .models import Jadwal
from django.contrib.admin import widgets
class DateInput(forms.DateInput):
	input_type = "date"
	
class TimeInput(forms.TimeInput):
	input_type = "time"

class MemJadwal(forms.ModelForm):
	class Meta:
		model = Jadwal
		fields = ['tanggal','hari','jam','kegiatan','tempat','kategori']
		widget = { 'tanggal' : DateInput(),
				'jam' : TimeInput()}

	
	