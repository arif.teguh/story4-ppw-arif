from django.conf.urls import url
from django.urls import include, path
from .views import index ,book1, book
urlpatterns = [
	path('', index, name='index'),
	path('book/', book1, name='book1'),
    path('request/', book),
]