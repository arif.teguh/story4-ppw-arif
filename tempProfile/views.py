from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import MemJadwal
from .models import Jadwal
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import JsonResponse
import requests
def index(request):
	return render(request,'story3.html')
def book1(request):
    return render(request, 'Book.html')

def book(request):
    books = list()
    response = requests.get('https://www.googleapis.com/books/v1/volumes?q=quilting')
    data = response.json()
    for i in range(10):
        books.append({
            'id': data['items'][i]['id'],
            'image': data['items'][i]['volumeInfo']['imageLinks']['thumbnail'],
            'title': data['items'][i]['volumeInfo']['title'],
            'author': data['items'][i]['volumeInfo']['authors'][0],
            'publisher': data['items'][i]['volumeInfo']['publisher'],
        })
    return JsonResponse({'books': books})

